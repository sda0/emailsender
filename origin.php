<?php
/**
 * Created by PhpStorm.
 * User: dscheglov
 * Date: 06.09.17
 * Time: 21:50
 */

abstract class Message
{
    public $to;
    public $from;
    public $subject;
    public $template;
    public $message;
    abstract public function send();
    public function getMessage()
    {
        return strtr($this->template, array('%message%' => $this->message));
    }
}

class HtmlMessage extends Message
{
    public $template = '<font size="12">%message%</font>';
    public function send()
    {
        mail($this->to, $this->subject, $this->getMessage(), "From: {$this->from}");
    }
}