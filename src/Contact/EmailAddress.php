<?php
namespace Step2\Contact;


use Step2\IAddress;

class EmailAddress implements IAddress {
    /** @var string  */
    protected $email;

    /** @var null|string  */
    protected $nickname;

    /**
     * @param $email
     * @param $nickname
     */
    public function __construct(string $email, $nickname = null) {
        $this->email = filter_var($email, FILTER_SANITIZE_EMAIL);
        $this->nickname = $nickname;
    }

    /**
     * @return null|string
     */
    public function getFormattedAddress() {
        if (!$this->validate()) {
            return null;
        }
        if (null === $this->nickname) {
            return $this->email;
        }
        return sprintf('%s <%s>', $this->nickname, $this->email);
    }

    /**
     * @return bool
     */
    public function validate() {
        return (bool) filter_var($this->email, FILTER_VALIDATE_EMAIL);
    }
}