<?php
namespace Step2;


interface IMessageSender
{
    public function send();
}