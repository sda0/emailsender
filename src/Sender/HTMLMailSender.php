<?php
namespace Step2\Sender;

use Step2\IMessage;
use Step2\IMessageSender;
use Step2\ITemplateEngine;
use Step2\TemplateEngine\HTMLMailTemplate;

class HTMLMailSender implements IMessageSender
{

    /** @var IMessage $message */
    protected $message;
    /** @var ITemplateEngine $bodyBuilder */
    protected $messageBuilder;
    /** @var  string */
    protected $headers;

    /**
     * HtmlMailSender constructor.
     * @param $message
     * @param ITemplateEngine $messageBuilder
     */
    public function __construct(IMessage $message, ITemplateEngine $messageBuilder = null) {
        $this->message = $message;
        $this->messageBuilder = $messageBuilder ?? new HTMLMailTemplate();
        $this->headers = "From: " . $message->getFrom();
    }

    /**
     * @param ITemplateEngine $messageBuilder
     * @return $this
     */
    public function setMessageBuilder(ITemplateEngine $messageBuilder) {
        $this->messageBuilder = $messageBuilder;
        return $this;
    }

    /**
     * @param string $headers
     * @return $this
     */
    public function setHeaders(string $headers) {
        $this->headers = $headers;
        return $this;
    }

    /**
     * @return bool */
    public function send() {
        $msg = $this->message;
        $msgBody = $this->messageBuilder->render($msg->getBody());
        return mail($msg->getReceivers(), $msg->getSubject(), $msgBody, $this->headers);
    }
}