<?php
namespace Step2\TemplateEngine;

use Step2\ITemplateEngine;

class HTMLMailTemplate implements ITemplateEngine
{
    /** @var  string */
    protected $template;

    /**
     * HTMLMailTemplate constructor.
     * @param string $template
     */
    public function __construct(string $template = '<font size="12">%message%</font>') {
        $this->template = $template;
    }

    public function render(string $message) : string {
        return strtr($this->template, ['%message%' => $message]);
    }

}