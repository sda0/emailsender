<?php
namespace Step2;


interface IAddress
{
    public function getFormattedAddress();
}