<?php
namespace Step2;


interface ITemplateEngine
{
    public function render(string $message) : string;
}