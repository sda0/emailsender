<?php
namespace Step2\Message;

use Step2\IAddress;
use Step2\IMessage;

class Message implements IMessage
{
    /** @var  IAddress[]  */
    protected $receivers = [];
    /** @var  IAddress */
    protected $from;
    /** @var  string */
    protected $subject;
    /** @var  string */
    protected $body;

    /**
     * @return IAddress[]
     */
    public function getReceivers(): array {
        return $this->receivers;
    }

    /**
     * @param IAddress $receiver
     * @return $this
     */
    public function addReceiver(IAddress $receiver = null) {
        if ($receiver !== null) {
            $this->receivers[] = $receiver;
        }
        return $this;
    }

    /**
     * @return string
     */
    public function getFrom(): string {
        return $this->from->getFormattedAddress();
    }

    /**
     * @param IAddress $from
     * @return $this
     */
    public function setFrom(IAddress $from) {
        $this->from = $from;
        return $this;
    }

    /**
     * @return string
     */
    public function getSubject(): string {
        return $this->subject;
    }

    /**
     * @param string $subject
     * @return $this
     */
    public function setSubject(string $subject) {
        $this->subject = $subject;
        return $this;
    }

    /**
     * @return string
     */
    public function getBody(): string {
        return $this->body;
    }

    /**
     * @param string $body
     * @return $this
     */
    public function setBody(string $body) {
        $this->body = $body;
        return $this;
    }
}