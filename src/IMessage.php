<?php
namespace Step2;


interface IMessage
{
    /**
     * @return IAddress[]
     */
    public function getReceivers(): array;

    /**
     * @param IAddress $receiver
     * @return $this
     */
    public function addReceiver(IAddress $receiver = null);

    /**
     * @return string
     */
    public function getFrom(): string;

    /**
     * @return string
     */
    public function getSubject(): string ;

    /**
     * @return string
     */
    public function getBody(): string;
}