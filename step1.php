<?php
namespace Step1;
/**
 * Created by PhpStorm.
 * User: dscheglov
 * Date: 06.09.17
 * Time: 22:17
 */
class EmailAddress {
    /** @var string  */
    protected $email;

    /** @var null|string  */
    protected $nickname;

    /**
     * @param $email
     * @param $nickname
     */
    public function __construct(string $email, $nickname = null) {
        $this->email = filter_var($email, FILTER_SANITIZE_EMAIL);
        $this->nickname = $nickname;
    }

    /**
     * @return null|string
     */
    public function getReceiver() {
        if (!$this->validate()) {
            return null;
        }
        if (null === $this->nickname) {
            return $this->email;
        }
        return sprintf('%s <%s>', $this->nickname, $this->email);
    }

    /**
     * @return bool
     */
    public function validate() {
        return (bool) filter_var($this->email, FILTER_VALIDATE_EMAIL);
    }
}

class Message
{
    /** @var  EmailAddress[]  */
    protected $receivers = [];
    /** @var  EmailAddress */
    protected $from;
    /** @var  string */
    protected $subject;
    /** @var  string */
    protected $body;

    /**
     * @return EmailAddress[]
     */
    public function getReceivers(): array {
        return $this->receivers;
    }

    /**
     * @param EmailAddress $receiver
     * @return $this
     */
    public function addReceiver(EmailAddress $receiver = null) {
        if ($receiver !== null) {
            $this->receivers[] = $receiver;
        }
        return $this;
    }

    /**
     * @return string
     */
    public function getFrom(): string {
        return $this->from->getReceiver();
    }

    /**
     * @param EmailAddress $from
     * @return $this
     */
    public function setFrom(EmailAddress $from) {
        $this->from = $from;
        return $this;
    }

    /**
     * @return string
     */
    public function getSubject(): string {
        return $this->subject;
    }

    /**
     * @param string $subject
     * @return $this
     */
    public function setSubject(string $subject) {
        $this->subject = $subject;
        return $this;
    }

    /**
     * @return string
     */
    public function getBody(): string {
        return $this->body;
    }

    /**
     * @param string $body
     * @return $this
     */
    public function setBody(string $body) {
        $this->body = $body;
        return $this;
    }
}

/**
 * @todo move $template to templateEngine strategy
 */
class HtmlSender
{
    /** @var Message $message */
    protected $message;
    /** @var string $template */
    protected $template;
    /** @var  string */
    protected $headers;

    /**
     * HtmlSender constructor.
     * @param $message
     * @param string $template
     */
    public function __construct(Message $message, string $template = '<font size="12">%message%</font>') {
        $this->message = $message;
        $this->template = $template;
        $this->headers = "From: " . $message->getFrom();
    }

    /**
     * Message builder
     * @return bool */
    public function send() {
        $msg = $this->message;
        /** @todo move it to strategy */
        $msgBody = strtr($this->template, ['%message%' => $this->message]);
        return mail($msg->getReceivers(), $msg->getSubject(), $msgBody, $this->headers);
    }
}